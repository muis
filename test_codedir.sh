if
test -z $1
then
CODE=code/*.c
else
CODE=$1
fi
echo $CODE

for line in $CODE
do
	echo testing $line
	cp $line test.c
	./run-test-on-test.sh && echo $line succeeded || echo $line failed
	rm -fr ref opt ref.s opt.s test.ref test.opt test.c
done
