#!/bin/bash
# Compiles the file test.c and checks the output of the reference with the
# optimised version of the program. muis is the optimiser for this.


CC=xgcc
I386CC=gcc
CFLAGS=-O0
SIM=sim-outorder
OPT=~/muis/muis/muis.py

X=true


cat code/supported.s | while read LINE
do
echo "testing $LINE"
cat code/skeleton.head > ref.s
echo "$LINE" >> ref.s
cat code/skeleton.foot >> ref.s
$OPT ref.s opt.s || X=false

if $X
then

	$CC $CFLAGS opt.s -o opt

	$SIM ref > test.ref 2>/dev/null || true
	$SIM opt > test.opt 2>/dev/null || true

	test -n "`diff test.ref test.opt`" && X=false
fi

rm -fr ref opt ref.s opt.s test.ref test.opt test.c
done
$X
