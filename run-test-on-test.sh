#!/bin/bash
# Original author: Nido Media
# Patches by: 
#    ...

# depends on the simplescalar sim-outorder MIPS emulator. Any suggestions
# welcome to change this

# Compiles the file test.c and checks the output of the reference with the
# optimised version of the program. muis is the optimiser for this.
CC=xgcc
I386CC=gcc
CFLAGS=-O0
SIM=sim-outorder
OPT=~/muis/muis/muis.py
OPTFLAGS=--agressive
X=true

$CC $CFLAGS -S test.c -o ref.s
$CC $CFLAGS ref.s -o ref

$OPT $OPTFLAGS ref.s opt.s >/dev/null || X=false

if $X
then
	$CC $CFLAGS opt.s -o opt

	$SIM ref > test.ref 2>/dev/null || true
	$SIM opt > test.opt 2>/dev/null || true

	test -n "`diff test.ref test.opt`" && X=false
fi

$X
