Source of these benchmarks is fuzzy. These are 'official' benchmarks
used during the computer science course to test the performance.
Replacement with proven public domain code or rediscovering licensing
information about these files is advised.
