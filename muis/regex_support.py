"""
The regex_support class is a class which contains a few variables which resemble
useful regular expressions for, among things, instruction recognition.

Code released under a free software compatible license. See LICENSE for more
details.
Original author: Nido Media
Code patches by:
	...
"""

import re

"""An arbitrary abount of whitespace"""
space = "\s*"

"""A variable name"""
name = "(?P<name>[\$\.\w]+)"

"""A comma, optinally surrounded by space"""
comma = space + "," + space

"""A number, decimal or hexadecimal"""
number = "-{0,1}(0x){0,1}[1234567890abcdef]+"
immediate = space + "(?P<immediate>" + number + ")" + space
offset = space + "(?P<offset>\${0,1}[\.\w]+([+-]" + number + "){0,1})" + space

branch_jumppoint_matcher = re.compile(r'([^,]+,)*' + name + space)

iteration_finder = re.compile(space + "--iterations=(?P<number>" + number + ")")

# regex for getting simulation results
simulation = re.compile("sim_cycle" + space + "(?P<result>" + number + ")" + space)

def regex_register(name):
	"""
	A regex_register is a construct with returns a register recognising regular
	expression string which names it according to the input.
	"""
	return "\s*(?P<" + name + ">\$\w+)\s*"

rt_register = regex_register("rt")
rs_register = regex_register("rs")
rd_register = regex_register("rd")
rw_register = regex_register("rw")

rdrs_registers = rd_register + comma + rs_register
rsrt_registers = rs_register + comma + rt_register
rdrsrt_registers = rd_register + comma + rsrt_registers

rdi_registers = rd_register + comma + immediate
rdrsi_registers = rd_register + comma + rs_register + comma + immediate

offset_rs = space + offset + "(\(" + rs_register + "\)){0,1}" + space

rdors_registers = rd_register + comma + offset_rs
rtors_registers = rt_register + comma + offset_rs


def escape(string):
	""" Escapes replaces the dot (.) character in a string with an escaped dot
		(\.), since this is the only character in instruction names which can
		pose a problem."""
	string = string.replace(".", "\.")
	return string

def rdrsrw_instruction(name):
	""" specially designed for the trunc instructions not in TSSTS, uses the rw
		register which is used as a temporary value."""
	name = escape(name)
	return(space + name + space + rdrs_registers + comma + rw_register)


def rsrd_instruction(name):
	""" Specially designed for the MTC1 instruction, which defies register
	order"""
	name = escape(name)
	return(space + name + space + rs_register + comma + rd_register)

def rdrs_instruction(name):
	name = escape(name)
	return(space + name + space + rdrs_registers)

def rdrsrt_instruction(name):
	name = escape(name)
	return(space + name + space + rdrsrt_registers)

def rsrt_instruction(name):
	name = escape(name)
	return(space + name + space + rsrt_registers)

def rdi_instruction(name):
	name = escape(name)
	return(space + name + space + rdi_registers)

def rdrsi_instruction(name):
	name = escape(name)
	return(space + name + space + rdrsi_registers)

def ors_instruction(name):
	name = escape(name)
	return(space + name + space + offset_rs)

def rdors_instruction(name):
	name = escape(name)
	return(space + name + space + rdors_registers)

def rtors_instruction(name):
	name = escape(name)
	return(space + name + space + rtors_registers)
