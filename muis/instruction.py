
"""
This python module contains models for all instructions.

TODO: Right now, this code contains the instruction set for the MIPS
SimpleScalar processor. This should be externalised and swappable with
other instruction sets later.

Code released under a free software compatible license. See LICENSE for more
details.
Original author: Nido Media
Code patches by:
	...
"""

import sys
import re
import aliasdb
import convenience
#hopefully deprecated soon
from regex_support import space, comma, rd_register, offset, rdors_registers, rdi_registers, rt_register, offset_rs, rdrsi_registers, rdrsrt_registers, rs_register, rdrs_registers, number, rtors_registers
import regex_support as rgx

"""The (integer) value of the most significant bit."""
most_significant_bit = 2147483648
"""The overflow value is one more then the highest respresentable number."""
overflow_value = 4294967296

def hex_to_decimal(value):
	return int(value, 16)

def calculate(string):
	"""
	Makes sure a given value is of an integer format. Also takes care of
	hexadecimal notation if needed.
	"""
	if string.__class__ == int:
		return string
	if "0x" == string[0:2]:
		string = hex_to_decimal(string)
	integer = int(string)
	return integer

def test(operation, string):
	""" Tests if the given string is an instruction of the class operation. Also,
	this function will create a field "signature" in the given operation class.
	This class will contain be the compuiled regular expression "regex", which
	is assumed available in the class.
	"""
	result = None
	if operation.signature == None:
		operation.signature = re.compile(operation.regex)

	match = operation.signature.match(string)
	if match:
		result = operation(match)
	return result

class instruction(object):
	""" The model for every instruction. Each instruction is a subclass of this superclass."""
	signature = None

	def __init__(self, match, reads = [], writes = []):
		""" Initialises the instruction and basically makes sure all values are
			set. It fills in the destination register (rd) and temporary
		   register (rw) in the write fields and adds the input registers (rs, 
		   rt) to the read fields."""
		self.regex_match = match
		self.read = []
		self.write = []
		self.immediate = None

		if 'immediate' in match.groupdict():
			self.immediate = match.group('immediate')
			if "0x" == self.immediate[0:2]:
				self.immediate = hex_to_decimal(self.immediate)
		if 'rs' in match.groupdict():
			# rs is the source register, so this is read
			self.read_add('rs')
		if 'rt' in match.groupdict():
			# rt is the second source register. So naturally, it is read.
			self.read_add('rt')
		if 'rd' in match.groupdict():
			# rd is the destination register, so that will be written
			self.write_add('rd')
		if 'rw' in match.groupdict():
			# rw is a temporary register, which will be written, but should be
			# dead
			self.write_add('rw')
		if	"offset" in match.groupdict():
			self.immediate = match.group("offset")

	def is_rs_computable(self):
		""" Determine wether the result of this function is statically
			computable given the one source register (rs) is given."""
		result = False
		if self.__class__.__dict__.has_key("compute_rs"):
			result = True
		return result

	def is_rsrt_computable(self):
		""" determine whether or not the result of this function is statically
			computable given two source registers (rs and rt)."""
		result = False
		if self.__class__.__dict__.has_key("compute_rsrt"):
			result = True
		return result

	def is_register_computable(self):
		""" Determine if this instruction is able to calculate a
		result given the register table."""
		result = False
		if self.__class__.__dict__.has_key("compute_registers"):
			result = True
		return result

	def read_add(self, string):
		""" shortcut for adding a match variable to the read list."""
		self.read.append(self.regex_match.group(string))
	
	def write_add(self, string):
		""" shortcut for adding a match variable to the write list."""
		self.write.append(self.regex_match.group(string))
	
	def has_immediate(self):
		""" checks wether said field has an immediate value set."""
		result = False
		if self.immediate != None:
			result = True
		return result
		
	def type(self):
		""" Returns the type of instruction this is.

			Practically, it returns the class of the object. Since usage of __
			functions is discouraged, the type function has been introduced to
			concentrate all __class__ calls to this (changable) point in the
			code."""
		return self.__class__

	def __str__(self):
		return self.regex_match.string

class load_address(instruction):
	"""
	Not defined in TSSTS. Assumed to be "Load Address", get the location of an
	address into memory. Since no promises can be made about this address, no
	worrying about it has to be done.
	"""	
	regex = space + "la" + space + rd_register + comma + offset
	def __init__(self, match):
		instruction.__init__(self, match)
		self.immediate = match.group("offset")
		self.write.append(match.group("rd"))
		
class load_byte_signed_displaced_addressing(instruction):
	regex = rgx.rdors_instruction("lb")

# Not defined in TSSTS
class load_immediate(instruction):
	regex = rgx.rdi_instruction("li")
	
class load_byte_unsigned_displaced_addressing(instruction):
	regex = rgx.rdors_instruction("lbu")

class load_word_displaced_addressing(instruction):
	regex = rgx.rdors_instruction("lw")

class double_load_word_displaced_addressing(instruction):
	regex = rgx.rdors_instruction("dlw")

class load_word_into_floating_point_register_file_displaced_addressing(instruction):
	regex = rgx.rdors_instruction("l.s")

class load_double_word_into_floating_point_register_file_displaced_addressing(instruction):
	regex = rgx.rdors_instruction("l.d")

class store_byte_displaced_addressing(instruction):
	regex = rgx.rtors_instruction("sb")
	
class store_word_displaced_addressing(instruction):
	regex = rgx.rtors_instruction("sw")
	
class double_store_word_displaced_addressing(instruction):
	regex = rgx.rtors_instruction("dsw")

class double_store_zero_displaced_addressing(instruction):
	regex = rgx.ors_instruction("dsz")
	
class store_word_from_floating_point_register_file_displaced_addressing(instruction):
	regex = rgx.rtors_instruction("s.s")
	
class store_double_word_from_floating_point_register_file_displaced_addressing(instruction):
	regex = rgx.rtors_instruction("s.d")

class add_immediate_unsigned_no_overflow_check(instruction):
	regex = rgx.rdrsi_instruction("addu")
	def compute_rs(self, rs):
		result = rs + self.immediate
		if result > overflow_value:
			result -= overflow_value
		return result
	
class add_unsigned_no_overflow_check(instruction):
	regex = rgx.rdrsrt_instruction("addu")
	def compute_rsrt(self, rs, rt):
		result = rs + rt
		if result > overflow_value:
			result -= overflow_value
		return result

class subtract_unsigned_without_underflow_check(instruction):
	regex= rgx.rdrsrt_instruction("subu")
	def compute_rsrt(self, rs, rt):
		result = rs - rt
		if result < 0:
			result += overflow_value
		return result
	
class subtract_immediate_unsigned_without_overflow_check(instruction):
	regex= rgx.rdrsi_instruction("subu")
	def compute_rs(self, rs):
		result = rs - self.immediate
		if result < 0:
			result += overflow_value
		return result
	
class multiply_signed(instruction):
	regex = rgx.rsrt_instruction("mult")
	def __init__(self, match):
		instruction.__init__(self, match)
		self.write.append("lo")
		self.write.append("hi")
		#TODO: make compute on register base
#	def compute_rsrt(self, rs, rt):
#		return calculate(rs) * calculate(rt)

class divide_signed(instruction):
	""" WARNING! This instruction has been implemented differently as defined in
	   	the SimpleScalar Tool Set, Version 2.0 by Doug Burger and Todd M Austin
	   	(TSSTS).  This because the code generated by GNU C 2.7.2.3 differs from
	   	said example.  This instruction does not assume it writes in the 'lo'
	   	and 'hi' registers."""
	regex="\sdiv\s*" + rdrsrt_registers
	def compute_rsrt(self, rs, rt):
		return rs / rt

class divide_unsigned(instruction):
	"""See divide_signed"""
	regex="\s*divu\s*" + rdrsrt_registers
	def compute_rsrt(self, rs, rt):
		rs / rt

class move_from_lo_register(instruction):
	regex = "\s*mflo\s*" + rd_register
	def __init__(self, match):
		instruction.__init__(self, match)
		self.read.append("lo")

class move_from_hi_register(instruction):
	regex = "\s*mfhi\s*" + rd_register
	def __init__(self, match):
		instruction.__init__(self, match)
		self.read.append("hi")

class move_to_hi_register(instruction):
	regex = "\s*mthi\s*" + rd_register
	
class move_to_lo_register(instruction):
	regex = "\s*mtlo\s*" + rd_register

class logical_and(instruction):
	regex = "\s*and\s*" + rdrsrt_registers
	def compute_rsrt(self, rs, rt):
		if rs < 0:
			rs += overflow_value
		if rt < o:
			rt += overflow_value
		return rs & rt
	
class logical_and_immediate(instruction):
	regex = rgx.rdrsi_instruction("andi")
	def compute_rs(self, rs):
		if rs < 0:
			rs += overflow_value
		immediate = self.immediate
		if immediate < 0:
			immediate += overflow_value
		return rs & immediate

class logical_or(instruction):
	regex = "\s*or\s*" + rdrsrt_registers
	def compute_rsrt(self,rs,rt):
		if rs < 0:
			rs += overflow_value
		if rt < o:
			rt += overflow_value
		return rs | rt

class logical_or_immediate(instruction):
	regex = "\s*ori\s*" + rdrsi_registers
	def compute_rs(self, rs):
		if rs < 0:
			rs += overflow_value
		immediate = self.immediate
		if immediate < 0:
			immediate += overflow_value
		return rs | self.immediate

class logical_xor(instruction):
	regex = rgx.rdrsrt_instruction("xor")
	def compute_rsrt(self, rs, rt):
		if rs < 0:
			rs += overflow_value
		if rt < o:
			rt += overflow_value
		return rs ^ rt

class logical_xor_immediate(instruction):
	regex = rgx.rdrsi_instruction("xori")
	def compute_rs(self, rs):
		if rs < 0:
			rs += overflow_value
		immediate = self.immediate
		if immediate < 0:
			immediate += overflow_value
		return rs ^ self.immediate

# move was not in the TSSTS document
class move(instruction):
	regex = "\s*move\s*" + rdrs_registers
	def compute_rs(self, rs):
		return rs

#rem was not in the TSSTS document. Assumed to work like divide
class rem(instruction):
	regex = "\s*rem\s*" + rdrsrt_registers
	def compute_rsrt(self, rs, rt):
		return rs % rt
	
#remu was not in the TSSTS document. Assumed to work like divide
class remu(instruction):
	regex = "\s*remu\s*" + rdrsrt_registers
	def compute_rsrt(self, rs, rt):
		rd = rs % rt
		if rd < 0:
			rd = rd + rs
		return rd

class shift_left_logically(instruction):
	regex = "\s*sll\s*" + rdrsi_registers
	def compute_rs(self, rs):
		return rs << self.immediate
	
class shift_right_logically(instruction):
	regex = "\s*srl\s*" + rdrsi_registers
	def compute_rs(self, rs):
		if rs < 0:
			# adding the overflow value, for with positive values the arithmatic
			# and logical shifts are the same. (python implements artihmatic
			# shifts)
			rs += overflow_value
		result = rs >> self.immediate
		return result

class shift_right_arithmatically(instruction):
	regex = "\s*sra\s*" + rdrsi_registers
	def compute_rs(self, rs):
		result = rs >> self.immediate
		return result

class set_less_then_immediate(instruction):
	regex = "\s*slt\s*" + rdrsi_registers
	def compute_rs(self, rs):
		if rs < immediate:
			return 1
		else:
			return 0

class set_less_then(instruction):
	regex = "\s*slt\s*" + rdrsrt_registers
	def compute_rsrt(self, rs, rt):
		if rs < rt:
			return 1
		else:
			return 0
	
class set_less_then_unsigned(instruction):
	regex = "\s*sltu\s*" + rdrsrt_registers
	def compute_rsrt(self):
		if rs < 0:
			rs += overflow_value
		if rt < 0:
			rt += overflow_value
		if rs < rt:
			return 1
		else:
			return 0
	
	
class set_less_then_unsigned_immediate(instruction):
	regex="\s*sltu\s*" + rdrsi_registers
	def compute_rs(self, rs):
		immediate = self.immediate
		if rs < 0:
			rs += overflow_value
		if immediate < 0:
			immediate += overflow_value
		if rs < rt:
			return 1
		else:
			return 0
	
class add_floating_point_single_precision(instruction):
	regex = rgx.rdrsrt_instruction("add.s")
	def compute_rsrt(self, rs, rt):
		return( rs + rt)

class add_floating_point_double_precision(instruction):
	regex = rgx.rdrsrt_instruction("add.d")
	def compute_rsrt(self, rs, rt):
		return ( rs + rt )

class subtract_floating_point_single_precision(instruction):
	regex = rgx.rdrsrt_instruction("sub.s")
	def compute_rsrt(self, rs, rt):
		return (rs - rt)

class subtract_floating_point_double_precision(instruction):
	regex = rgx.rdrsrt_instruction("sub.d")
	def compute_rsrt(self, rs, rt):
		return (rs - rt)

class multiply_floating_point_single_precision(instruction):
	regex = rgx.rdrsrt_instruction("mul.s")
	def compute_rsrt(self, rs, rt):
		return (rs * rt)

class multiply_floating_point_double_precision(instruction):
	regex = rgx.rdrsrt_instruction("mul.d")
	def compute_rsrt(self, rs, rt):
		return (rs * rt)

class divide_floating_point_single_precision(instruction):
	regex = rgx.rdrsrt_instruction("div.s")
	def compute_rsrt(self, rs, rt):
		return (rs / rt)

class divide_floating_point_double_precision(instruction):
	regex = rgx.rdrsrt_instruction("div.d")
	def compute_rsrt(self, rs, rt):
		return (rs / rt)

class absolute_value_single_precision(instruction):
	regex = rgx.rdrs_instruction("abs.s")
	def compute_rs(self, rs):
		return (abs(x))

class absolute_value_double_precision(instruction):
	regex = rgx.rdrs_instruction("abs.d")
	def compute_rs(self, rs):
		return (abs(x))

class move_floating_point_value_single_precision(instruction):
	regex = rgx.rdrs_instruction("mov.s")
	def compute_rs(self, rs):
		return(rs)

class move_floating_point_value_doube_precision(instruction):
	regex = rgx.rdrs_instruction("mov.d")
	def compute_rs(self, rs):
		return(rs)


class negate_floating_point_value_single_precision(instruction):
	regex = rgx.rdrs_instruction("neg.s")
	def compute_rs(self, rs):
		return(-rs)

class negate_floating_point_value_double_precision(instruction):
	regex = rgx.rdrs_instruction("neg.d")
	def compute_rs(self, rs):
		return(-rs)

class convert_double_precision_to_single_precision(instruction):
	regex = rgx.rdrs_instruction("cvt.s.d")
	def compute_rs(self, rs):
		return(rs)

class convert_integer_to_singple_precision(instruction):
	regex = rgx.rdrs_instruction("cvt.s.w")
	def compute_rs(self, rs):
		# the assumption made here is that this computation is
		# only done on integer arguments. If the input is (from
		# the standpoint of the optimiser) a floating point
		# number, this instruction returns incorrect results.
		return ( float(rs))

class convert_single_precision_to_double_precision(instruction):
	regex = rgx.rdrs_instruction("cvt.d.s")
	def compute_rs(self, rs):
		return ( float(rs))

class convert_integer_to_double_precision(instruction):
	regex = rgx.rdrs_instruction("cvt.d.w")
	def compute_rs(self, rs):
		return( float(rs))

class convert_single_precision_to_integer(instruction):
	regex = rgx.rdrs_instruction("cvt.w.s")
	def compute_rs(self, rs):
		# the assumption made here is that this computation here
		# is made only on floating point numbers. If done on
		# integer numbers, the result will be incorrect
		return( int(rs))

class convert_double_precision_to_integer(instruction):
	regex = rgx.rdrs_instruction("cvt.w.d")
	def compute_rs(self, rs):
		return( int(rs))

class fcc_instruction(instruction):
	"""An instruction which needs to be approved by the Federal Communications
	Commission.

	Not really. Actually it's the kind of instruction that implicitly stores
	it's result in the $fcc register"""
	def __init__(self, match):
		instruction.__init__(self, match)
		# Do not use the write_add shortcut, because that expects a
		# register name (in example: 'rd', 'rs', 'rt')
		self.write.append("$fcc")

class test_if_equal_single_precision(fcc_instruction):
	regex = rgx.rsrt_instruction("c.eq.s")

class test_if_equal_double_precision(fcc_instruction):
	regex = rgx.rsrt_instruction("c.eq.d")
	
class test_if_less_then_single_precision(fcc_instruction):
	regex = rgx.rsrt_instruction("c.lt.s")

class test_if_less_then_double_precision(fcc_instruction):
	regex = rgx.rsrt_instruction("c.lt.d")

class test_if_tess_then_or_equal_single_precision(fcc_instruction):
	regex = rgx.rsrt_instruction("c.le.s")

class test_if_tess_then_or_equal_double_precision(fcc_instruction):
	regex = rgx.rsrt_instruction("c.le.d")

class square_root_single_precision(instruction):
	regex = rgx.rdrs_instruction("sqrt.s")

class square_root_double_precision(instruction):
	regex = rgx.rdrs_instruction("sqrt.d")

class move_from_floating_point_to_integer_register(instruction):
	regex = rgx.rdrs_instruction("mfc1")

class move_double_word_from_floating_point_to_general_purpose_register(instruction):
	"""Not in TSSTS"""
	regex = rgx.rdrs_instruction("dmfc1")

class move_from_integer_to_floating_point_register(instruction):
	regex = rgx.rsrd_instruction("mtc1")

class truncate_floating_point_single_precision(instruction):
	""" truncates the contents of the source floating point register and puts the
		resulting 32 bit signed integer in the destination floating point
		register, using a third, general purpose register to hold a temporary
		value."""
	regex = rgx.rdrsrw_instruction("trunc.w.s")

class truncate_floating_point_double_precision(instruction):
	regex = rgx.rdrsrw_instruction("trunc.w.d")

class no_operation(instruction):
	regex = space + "nop" + space

class macro(instruction):
	"""
A macro is an instruction which basically starts with a dot, and can do
anything. It does not define a read or write field.
	"""
	regex="\s*\..*"
	def __init__(self, match):
		instruction.__init__(self, match)
	
def is_number(string):
	"""	
	checks whether or not something is a number
	"""	
	value = False
	if  string.__class__ == int:
		value = True
	elif re.compile(number).match(string):
		value = True
	return value
	
def set():
	"""
	Creates a list of all instructions recognised by the instruction parser.
	It will process all subclasses of instruction. Using the 'leaf' classes as
	instructions in the set. For example. memory_instruction is a subclass of
	instruction. instruction is a subclass of memory_instruction.
	load_byte_ is a subclass of that. Only load_byte shows up in the instruction
	set.
	"""
	unprocessed_instructions = instruction.__subclasses__()
	instruction_set = []
	while len(unprocessed_instructions) != 0:
		item = unprocessed_instructions.pop()
		new_instructions = item.__subclasses__()
		if len(new_instructions) == 0:
			instruction_set.append(item)
		else:
			unprocessed_instructions += new_instructions
	return instruction_set

def find_instruction(string):
	"""
	finds out what is the instruction belonging to the string given.
	"""
	result = None
	instruction_list = set()
	while len(instruction_list) != 0:
		candidate = instruction_list.pop()
		result = test(candidate, string)
		if result:
			break
	return result

def parse(string):
	"""
Parses the given single string into a single specific type of instruction.
Returns either an instruction or None.
	"""
	alias = aliasdb.alias()

	result = find_instruction(string)

	if result == None:
		print >> sys.stderr, "none found: \"" + string + "\""
		sys.exit(1)

	if result.has_immediate():
		immediate = result.immediate
		if not (is_number(immediate) or alias.check(immediate)):
			print >> sys.stderr, ("macro alias not found: " + immediate)
			result = None
	
	return result

# vim: foldmethod=indent:foldlevel=0
