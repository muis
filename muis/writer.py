"""
Writer contains code to write assemly out to file and to change basic block
structures into assembly code.

Code released under a free software compatible license. See LICENSE for more
details.
Original author: Nido Media
Code patches by:
	...
"""

import basic_block
import convenience
import sys

def instructions_to_assembly(instructions):
	"""
	Turns the instructions given into lines of assembly code strings. Created
	mostly to take complexibility out of the blocks_to_assembly function. Takes
	advantage of the implementation of instruction.
	"""	
	assembly = []
	for line in instructions:
		assembly.append(line.regex_match.group())
	return assembly
		
def is_jump_needed(block, next):
	"""
	determines whether or not a jump is needed from this block given the next in
	line. If the jump target is equal to the next block label, no jump is
	needed. If the jump target is "", it is a fake jump target and no jump
	target is needed. This is given this block is at the end of the line though.
   	If the jump target and next block differ, the jump is
	needed.
	"""
	if ((None == next) and ("" == block.jump)):
		return False
	elif "" == block.jump:
		assert False, "FAKE JUMP IN THE MIDDLE OF THE CODE"
	elif None == next:
		return True

	if next.label != block.jump:
		return True
	else:
		return False
		

def structure_to_assembly(structure):
	"""
	This function takes the assembly/basic block structure and turns the blocks
	back to assembly. Then it will use the builtin assembly and the reconverted
	assembly to create a list fo assembly instructions. This is returned.
	"""
	result = []
	for tuple in structure:
		if tuple[0] == "assembly":
			result += tuple[1]
		elif tuple[0] == "block":
			result.append("\t.ent\t"+tuple[1])
			assembly = blocks_to_assembly(tuple[2])
			result += assembly
			result.append("\t.end\t"+tuple[1])
	return result

def blocks_to_assembly(basic_blocks):
	"""
	Turns a basic block list into a list of assembly instructions and can write
   	this to a file. The list of basic blocks is assumed to be in the right
	order. Also it is assumed excess labels are not a problem. excess jumps
   	(when the jump target is the next block) are removed.
	"""
	assembly = []
	block_count = len(basic_blocks)

	for block in basic_blocks:
		# Jump Label
		assembly.append(block.label+":")
		
		# Instructions
		if block.instructions != []:
			assembly += instructions_to_assembly(block.instructions)
		
		# insert the Jump And Link or Branch instruction
		if block.jal:
			assembly.append("\tjal\t" + block.jal)
		if block.branch:
			assembly.append(block.branch[0] + "\t" + block.branch[1])

		# Find out the next block which will be processed. This is important for
		# final jump optimalisation. No need to jump to the next block if normal
		# program flow brings you there on its own.
		next_block_index = basic_blocks.index(block) + 1
		next_block = None
		if next_block_index < block_count:
			next_block = basic_blocks[next_block_index]
		
		if is_jump_needed(block, next_block):
			# Jump Instruction
			assembly.append("\tj " + block.jump)
	return assembly
		
def write(filename, block_structure):
	"""
	takes a basic block structure and writes it to given file. Overwrites file
	without questioning.
	"""
	assembly = structure_to_assembly(block_structure)
	string = ""
	for line in assembly:
		string += line + "\n"

	output = open(filename, "w")
	output.write(string)
	output.close()


# vim:nocindent:nosmartindent:autoindent:tabstop=4:shiftwidth=4
# vim:textwidth=80:foldmethod=indent:foldlevel=0
