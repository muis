"""
Convenience functions mainly for debugging purposes

This class is created purely for the convenience of programming/debugging. It
contains contructs like "press enter to continue" and that sort of stuff,
without bothing the code itself too much. When completed, this module shouldn't
be loaded in the code and could be removed. It's still here for the sake of it.

Code released under a free software compatible license. See LICENSE for more
details.
Original author: Nido Media
Code patches by:
	...
"""

import sys
import tempfile
import os

def press_enter():
	"""
	Asks for input. Is used just as a way to pause the function requesting the
	user to press enter to continue. Usefull to pause program flow at certain
	interresting moments
	"""
	raw_input('Press ENTER to continue...\n')

def get_tempfile(extension = ""):
	""" Creates a temp file and returns it's name"""
	temp = tempfile.mkstemp(extension)
	filename = temp[1]
	os.close(temp[0])
	return filename

