#!/usr/bin/env python
# Code released under a free software compatible license. See LICENSE for more
# details.
# Original author: Nido Media
# Code patches by:
#	...


import unittest
import reader
import writer
import basic_block

class testWriter(unittest.TestCase):
	def setUp(self):
		self.testfile = "../code/basic_file.s"
		self.basic_blocks = reader.reader(self.testfile)

	def testWriting(self):

		self.assembly = writer.structure_to_assembly(self.basic_blocks)
		
if __name__ == '__main__':
	unittest.main()
