#!/usr/bin/env python
# Code released under a free software compatible license. See LICENSE for more
# details.
# Original author: Nido Media
# Code patches by:
#	...


import re
import sys
import unittest

import basic_block
import instruction
import reader

import convenience

whitespace = re.compile("^\s*$")
entscan = re.compile(".*\.ent.*")
endscan = re.compile(".*\.end.*")
	
class testReader(unittest.TestCase):
	def setUp(self):
		self.testfile = "../code/basic_file.s"
		
	def testReading(self):
		nonjumping_blocks = 0
		block_structure = reader.reader(self.testfile)

		used_labels = []
		for tuple in block_structure:
			if tuple[0] == "block":
				block_list = tuple[2]
				
				for block in block_list:
					self.assertTrue(block.label not in used_labels, "No double labels!")
					used_labels.append(block.label)

					self.assertTrue(block.jump, "This block has no jump! Not even a fake one")

					# only test blocks which have instructions
					if len(block.instructions) > 0:
						for line in block.instructions:
							line = line.regex_match.group()

							# There shall be no .ent and .end in the code
							self.assertFalse( entscan.match(line), "Found .ent!")
							self.assertFalse( endscan.match(line), "Found .end!")
						

if __name__ == '__main__':
	unittest.main()
