#!/usr/bin/env python
# Code released under a free software compatible license. See LICENSE for more
# details.
# Original author: Nido Media
# Code patches by:
#	...


import aliasdb
import unittest

class testAliasdb(unittest.TestCase):
	def setUp(self):
		self.alias = aliasdb.alias()
		
	def testAdding(self):
		self.alias.add("hallo")
		self.alias.add("world")
		self.assertTrue(self.alias.check("hallo"))
		self.assertTrue(self.alias.check("world"))

	def testAddingDoubles(self):
		self.assertTrue(self.alias.add("alias"))
		self.assertFalse(self.alias.add("alias"))
		self.assertTrue(self.alias.check("alias"))

	def testTwoAliases(self):
		self.second = aliasdb.alias()
		self.assertTrue(self.alias.add("double"))
		self.assertFalse(self.second.add("double"))
		self.assertTrue(self.alias.add("test2"))
		self.assertTrue(self.second.check("test2"))

	def testAlternateConstructor(self):
		self.assertTrue(aliasdb.alias("third"))

if __name__ == '__main__':
	unittest.main()
