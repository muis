"""
aliasdb holds a database (list) of possible aliases and functions supporting
this act.

Code released under a free software compatible license. See LICENSE for
more details.
Original author: Nido Media
Code patches by:
	...
"""
import re
from regex_support import space, name
import sys

def create_database(string_list):
	"""
	create_database accepts a list of (assembly instruction) strings and filters
	the aliases from them.
	"""
	for line in string_list:
		database = alias()
		database.find(line)


class alias:
	"""
	The alias class is made to keep track of all aliases. Aliases can be used
	instead of numbers in certain computations. Some of these aliases have
	special meaning, such as ".word", which next to being able to replace a
	number, also represent a memory location.

	Main use of this class is to allow for these aliases to exist without
	automagically accepting each string as an alias. This class allows for
	adding and checking of said aliases.
	"""

	# aliases is basically just a list of values
	aliases = []

	mask_regex = re.compile(space + "(?P<offset>\${0,1}[.\w]+)" + space)

	regex_list = [
			re.compile(space + "\.globl" + space + name + space),
			re.compile(space + "\.comm" + space + name + space),
			re.compile(space + "\.lcomm" + space + name + space),
			re.compile(space + "\.word" + space + name + space),
			re.compile(space + name + space + ":" + space)
	]

	def __init__(self, candidate=None):
		"""
		Creates an aliasdb instance.
		"""
		self.aliases = alias.aliases
		if candidate:
			self.add(candidate)

	def find(self, line):
		for regex in alias.regex_list:
			match = regex.match(line)
			if match:
				alias(match.group("name"))

	def check(self, candidate):
		"""
		Checks wether or not the candidate is in the alias database. Returns
		True on success, False on failure.
		"""
		result = False
	
		match = alias.mask_regex.match(candidate)
		if match:
			candidate = match.group()
	
		if candidate in self.aliases:
			result = True
		return result
	
	def add(self, value):
		"""
		Adds a certain value to the alias database. Returns True on succes,
		False on failure( I.E. when the value is already in the database.
		"""
		result = None
		if self.check(value):
			result = False
		else:
			self.aliases.append(value)
			result = True
		return result

# vim:nocindent:nosmartindent:autoindent:tabstop=4:shiftwidth=4
# vim:textwidth=80:foldmethod=indent:foldlevel=1
