#!/usr/bin/env python
"""
This is muis. My Um, Instruction Sequencer . Python Ycleped.

Muis is a peephole optimiser for MIPS assembly.  Specifically for the Simple
Scalar based input. It has been tested using GCC 2.7.2.3 as a compiler and
sim-outorder to test its workings. The real source of the name is the fact that
'peephole' sounds like 'piep' hole. 'piep' being the sound made by mice, and a
hole being the place where a mouse lives. When one translates 'mouse' to Dutch,
you get 'muis'.

Code released under a free software compatible license. See LICENSE for more
details.
Original author: Nido Media
Code patches by:
	...
"""

import reader
import writer
import regex_support as regex
import optimize
import options
import convenience

from sys import argv as command_line_arguments


def usage():
	"""
	Prints out how to use the program. Invoced when confused
	"""
	print """My Unique Instruction Superformanceoptimiser . Python Ycleped

This program optimises MIPS assembly code. To use it call this program, with the
assembly you want to optimise as first argument, and the output name as second
argument. The program will then read it and optimises the code according to its
internal rules.

Agressive mode (--agressive) assumes jump-and-link honors register allocation
conventions. Do not set this unless you are sure these convention are met.
Be warned. Compilers don't usually follow these conventions.

""" + command_line_arguments[0] + "[--agressive] [--iterations=number] input.s out_optimised.s"

def main():
	"""
	Processes the input file name and writes it to the output filename. This is
	the base function of the muis program.
	"""
	in_offset = 0
	out_offset = 1
	arg_offset = 1

	for arg in xrange(arg_offset, len(command_line_arguments)):
		argument = command_line_arguments[arg]
		print argument
		if argument == "--agressive":
			options.set("agressive")
			arg_offset += 1
		options.set("iterations", 5)
		if argument == "--iterations":
			result = regex.iteration_finder.match(argument)
			if result:
				options.set("iterations", int(result.group("number")))

	in_file = command_line_arguments[in_offset + arg_offset]
	out_file = command_line_arguments[out_offset + arg_offset]

	assembly = reader.reader(in_file)
	optimised_assembly = optimize.optimize_block_structure(assembly)
	writer.write(out_file, assembly)

	return 0

# Start the module if loaded itself.
if "__main__" == __name__:
	if len(command_line_arguments) < 3:
		usage()
	else:
		main()

# vim:nocindent:nosmartindent:autoindent:tabstop=4:shiftwidth=4
# vim:textwidth=80:foldmethod=indent:foldlevel=0
