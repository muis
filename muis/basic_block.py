"""
Contains functions and classes regarding basic blocks.

Code released under a free software compatible license. See LICENSE for more
details.
Original author: Nido Media
Code patches by:
	...
"""

import aliasdb
import options
import regex_support
import registers

"""
basic_block attempts to be a structure for basic blocks. It will inherit all
needed information from the instructions inserted.
"""

def print_block_list(blocks):
	"""
	Prints the basic blocks in the basic block structure. Also prints the
	auxilary assembly code
	"""
	for item in blocks:
		print item

def test_block(instructions):
	"""
	Create a new basic block with the given instructions. It omits other
	variables and is therefor not usefull outside of test functions.
	"""
	block = basic_block("test", instructions,"jump",None,None)
	return block

def attach_previous(block_list):
	"""This function takes a list of basic blocks and finds out which blocks are
	followed by the given block"""
	for block in block_list:
		for exit in block.exit_points():
			exit.preceding.append(block)

def reset_liveness(block_list):
	""" recalculates the liveness of all given basic blocks, taking following
	and previous blocks in account."""
	for block in block_list:
		block.reset_liveness()
	changed = True
	while changed == True:
		changed = update_liveness(block_list)

def get_block(block_list, string):
	""" takes the string representing a block name and returns a block with that
	name from the block_list. Returns None when nothing is found."""
	result = None
	for block in block_list:
		if block.label == string:
			result = block
	return result

def update_liveness(block_list):
	"""Updates the given block_list and returns wether or not this update has
	changed anything. The idea is to keep updating until no changes happen."""
	changed = False
	for block in block_list:
		# One caveat with this way of writing it down is that it doesn't quite
		# resemble the fact update_dead and update_live actually change the
		# block. 
		if block.update_dead(block_list):
			changed = True
		if block.update_live(block_list):
			changed = True
	return changed


class basic_block:
	def __init__(self, label, instructions, jump, branch, jal, block_list=None):
		""" Creates a new basic block given the input values"""
		self.label = label
		self.instructions = instructions
		self.jump = jump
		self.branch = branch
		self.jal = jal
		self.preceding = []
		self.constant_folding_registers = {}
		
		# Make sure liveness is correct (on this single block).
		self.reset_liveness()

		# Make sure the block is correct
		self.test_integrity()

		# The block_list is the block list to which this block belongs.
		self.block_list = block_list

		# The label can be used as alias for load instructions
		aliasdb.alias(label)

	def update_live(self, block_list):
		"""Updates the liveness of this block with respect to following blocks."""
		changed = False
		for follower in self.get_exit_points():
			if follower == "$31":
				for register in registers.jal_return_live:
					if register not in self.live:
						self.live.append(register)
						changed = True
			else:
				for register in follower.live:
					if register not in self.live:
						self.live.append(register)
						changed = True
		return changed

	def update_dead(self, block_list):
		"""Tries to update its own dead register information according to the
		information in following blocks."""
		changed = False
		# Create the list of potential dead registers
		new_dead = []
		for register in registers.all:
			if register not in self.live:
				new_dead.append(register)
		# Update the list to remove nondead registers
		for follower in self.get_exit_points():
			if follower == "$31":
				follower = registers.jal_return()
			for register in new_dead:
				if register not in follower.dead:
					new_dead.remove(register)
		# Update the internal list
		for register in new_dead:
			if register not in self.dead:
				self.dead.append(register)
				changed = True
		# return wether or not something changed
		return changed

	def reset_liveness(self):
		"""Resets the live and dead fields in the basic block. Beware that this
		resets the liveness according to this block alone. It does not take into
		account following blocks."""
		live_dict = self.liveness()
		self.live = live_dict['live']
		self.dead = live_dict['dead']
	
	def get_exit_points(self):
		""" Returns a list of exit points (basic blocks that are followed by
		this one. This does not include the Jump And Link exit point if it is
		there."""
		labels = []
		if self.jump:
			labels.append(self.jump)
		if self.branch:
			labels.append(self.get_branch_jumppoint())
		exits = []
		for label in labels:
			if label == "$31":
				# Special case: $31 is 'return' from JAL
				exits.append("$31")
			else:
				exits.append(get_block(self.block_list, label))
		return exits

	def get_branch_reads(self):
		""" Returns the registers read by this branch instruction."""
		if not self.branch:
			print sys.stderr, "NO BRANCH, YOU IDIOT!"
			return None
		arguments = self.branch[1].split(",")
		# split off the last argument, as it is the jump point
		registers = arguments[:-1]
		# take out excess whitespace
		for x in xrange(len(registers)):
			registers[x] = registers[x].strip()
		return registers

	def get_branch_jumppoint(self):
		""" Returns the name of the location where this branch will jump to."""
		if not self.branch:
			return None
		
		arguments = self.branch[1]
		match = regex_support.branch_jumppoint_matcher.match(arguments)
		if match == None:
			import sys
			print >> sys.stderr, arguments
			print >> sys.stderr, self.branch[0]
		return match.group('name')

	def liveness(self):
		"""Returns a dictionary. This dictionary contains a list of 'live'
		registers, which is a list of registers which are being read by this basic block before
		they are being written, and a list of 'dead' registers, which are the the registers
		being written before being read. The validity of the liveness is at the
		beginning of the function. As such, it should not be used within the
		function itself.
		
		This function takes into account the 'agressive' option invocation. If
		set, it is assumed some registers are dead and some are live if this
		block has a Jump And Link. Otherwise, all registers are assumed to be live.
		"""
		dict = { "live": [], "dead": [] }
		if self.instructions:
			for inst in self.instructions:
				for register in inst.read:
					if register not in dict["dead"]:
						dict["live"].append(register)
				for register in inst.write:
					if register not in dict["live"]:
						dict["dead"].append(register)

		if self.jal:
			jal_dict = registers.jal_liveness()
			# The following are interchangable, no register from the jal__registers
			# is both dead and alive at the same time.
			for register in jal_dict['dead']:
				if register not in dict['live']:
					dict['dead'].append(register)
			for register in jal_dict['live']:
				if register not in dict['dead']:
					dict['live'].append(register)

		return dict


	def test_integrity(self):
		"""
		Tests wether this basic block is okay or if it violates a basic block
		rule. Returns True if the block is valid, will exit with an assert if
		invalid. If the asserts are turned off somehow, it returns false.
		"""
		result = True
		if self.jal and self.branch:
			assert False, "JAL and branch cannot be in the same basic block"
			result = False
		if not self.jump:
			if self.jump != "":
				assert False, "Basic block has no jump instruction"
				result = False
		if not self.label:
			assert False, "The block has no name. It should have a name"
		return result

	def __str__(self):
		"""
		Returns a crude string representation of the Basic Block
		"""
		result = "Basic_block " + self.label + "\n"
		if self.instructions == None:
			result += "No Instructions\n"
		else:
			result += "Instructions\n"
			for instruction in self.instructions:
				result += "\t" + instruction.__str__() + "\n"

		if self.jal:
			result += "Jump And Link: " + self.jal + "\n"
		if self.branch:
			result += "Branches: " + self.branch + "\n"
		if self.jump:
			result += "Jumps: " + self.jump + "\n"
		return result

# vim:nocindent:nosmartindent:autoindent:tabstop=4:shiftwidth=4
# vim:textwidth=80:foldmethod=indent:foldlevel=0
