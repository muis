"""
This module handles registers. It has the lists of all available registers
and a list of Jump And Link related registers. Also functions related to
registers are included.

Code released under a free software compatible license. See LICENSE for more
details.
Original author: Nido Media
Code patches by:
	...
"""

import options


"""All registers used in MIPS. All live in a non-agressive setting"""
all= ["$0", "$1", "$2", "$3", "$4", "$5", "$6", "$7", "$8", "$9",
"$10", "$11", "$12", "$13", "$14", "$15", "$16", "$17", "$18", "$19", "$20",
"$21", "$22", "$23", "$24", "$25", "$26", "$27", "$28", "$sp", "$fp", "$31",
"$f0", "$f1", "$f2", "$f3", "$f4", "$f5", "$f6", "$f7", "$f8", "$f9", "$f10",
"$f11", "$f12", "$f13", "$f14", "$f15", "$f16", "$f17", "$f18", "$f19", "$f20",
"$f21", "$f22", "$f23", "$f24", "$f25", "$f26", "$f27", "$f28", "$f29", "$f30",
"$f31", "$fcc"]

"""If agressive, these registers are presumed live with a JAL."""
jal_live = ["$2", "$3", "$16", "$17", "$18", "$19", "$20", "$21",
"$22", "$23", "$30"]

"""If agressive, these registers are presumed dead with a JAL."""
jal_dead = ["$8", "$9", "$10", "$11", "$12", "$13", "$14", "$15",
"$24", "$25"]

jal_return_dead = []
jal_return_live = all
if options.is_set("agressive"):
	# upon return, the dead and life registers flip (save / saved registers)
	jal_return_dead = jal_live
	jal_return_live = jal_dead

class jal_return():
	live = jal_return_live
	dead = jal_return_dead

def jal_liveness():
	"""Calculates the liveness of this block's jump-and-link instruction.
	Returns None when no JAL is found."""
	dict = None
	dict = {}
	# Assume non-agressive settings first
	dict['dead'] = []
	dict['live'] =  all
	if options.is_set("agressive"):
		dict['dead'] = jal_dead
		dict['live'] = jal_live
	return dict

def get_intersection(list1, list2):
	""" Returns the list of all registers which intersects list1 and list2."""
	result = []
	for register in list1:
		if register in list2:
			result.append(register)
	return result

def get_union(list1, list2):
	""" Returns the union of both given lists (of registers), and removes excess
   	registers from the list"""
	result = []
	for register in list1 + list2:
		if register not in result:
			result.append(register)
	return result
