"""
Code to optimise the assembly instructions.

Code released under a free software compatible license. See LICENSE for more
details.
Original author: Nido Media
Code patches by:
	...
"""

import instruction
import options
import basic_block
import convenience
import registers
import regex_support as regex
import options
import writer
import aliasdb

import os
import subprocess
import tempfile
import sys

def instructions_are_equal(instruction_1, instruction_2):
	"""
	Returns True when the instructions given are of the same class and read and
	write the same registers. False otherwise.
	"""
	result = False
	if((instruction_1.type == instruction_2.type) and
			(instruction_1.read == instruction_2.read) and 
			(instruction_1.write == instruction_2.write)):
		result = True
		# Notable exception: memory instructions need to have the same offset
		if isinstance(instruction_1, instruction.memory_instruction):
			if instruction_1.offset != instruction_2.offset:
				result = False
	return result

def instructions_same(original, copy):
	"""
	This looks if the instructions are the same, differing only in their write
	fields. If two instructions have the same input but write different
	registers, one could be replaced by a copy instruction. Note this looks at
	instruction level only and not at what hapens within or outside the basic
	blocks.
	"""
	result = False
	if (( original.type == copy.type) and
			(original.read == copy.read)):
		result = True

		# notable exception: also immediate values have to be correct
		if original.has_immediate():
			if not original.immediate == copy.immediate:
				result = False

		# Notable exception, "macro's" (instructions starting with '.')
		# will not be changed.
		if isinstance(copy, instruction.macro):
			result = False
		
	return result
	
def reads_in_range_exclusive(basic_block, start, end):
	"""
	Returns all values read in the range start to end, excluding start and end
	as instructions themselves. E.G. reads_in_range_exclusive(block,1,3) will
	examine only instruction 2.
	"""
	reads = []
	set = basic_block.instructions[start + 1:end]
	for instruction in set:
		for read in instruction.read:
			reads.append(read)
	return reads
	
def writes_in_range_exclusive(basic_block, start, end):
	"""
	Analogous to reads_in_range_exclusive, only with the write variable.
	"""
	writes = []
	set = basic_block.instructions[start + 1: end]
	for instruction in set:
		for write in instruction.write:
			writes.append(write)
	return writes
	
def registers_written(registers, basic_block, start, end):
	"""
	Checks wether the registers in the basic block inbetween instructions start
   	and end in the basic block write given registers
	"""
	result = False
	writes = writes_in_range_exclusive(basic_block, start, end)
	for register in registers:
		if register in writes:
			result = True
	return result

def replace_instruction_by_move(instructions, register, location):
	"""
	This function replaces a second instruction with a move instruction. It
	assumes all checks have been done and will continue even when this is a
	bogus operation. It will create a move from the start instruction
	destination register to the location instruction destination register. The
	assumption is made various 'temp' registers are not important.
	"""
	string = ("move " + instructions[location].regex_match.group("rd") + "," +
			register)
	new_instruction = instruction.parse(string)
	instructions[location] = new_instruction
	
def instruction_is_noop(op):
	""" 
	Returns true if this operation does nothing to alter the register state,
	thus having no effect on the execution of the program other then taking up
	time and space.

	TODO: move to instruction
	"""
	value = False
	if op.type == instruction.move:
		if op.read == op.write:
			value = True
	elif (op.type == instruction.logical_or_immediate or
			op.type == instruction.add_immediate_unsigned_no_overflow_check or
			op.type == instruction.logical_or_immediate or
			op.type == instruction.subtract_immediate_unsigned_without_overflow_check):
		if op.immediate == 0:
			value = True
	return value
	
def common_subexpression_elimination(basic_block):
	"""
	The idea of common subexpression elimination is to take the instructions
	which do the same thing and replace the latter one with a copying
	instruction because copy instructions are generally cheaper or (at least) as
   	cheap as computations.
	"""
	instructions = basic_block.instructions
	instruction_count = len(basic_block.instructions)
	
	for end in xrange(instruction_count - 1, -1, -1):
		
		# Instruction to find copies of.
		work_instruction = instructions[end]
		name = instructions[end].type
		read = instructions[end].read
		write = instructions[end].write
		
		for start in xrange(end - 1, -1, -1):
			# Potential copy of said instruction.
			if instructions_same(instructions[start],
					work_instruction):
				if not registers.get_intersection(read, instructions[start].write):
					replace_instruction_by_move(instructions,
							start.regex_match.group("rd"), end)

			if registers.get_union(read, instructions[start].write):
				# Registers read by this potential changing register are
				# overwritten, so no valid common subexpression can be found
				# now.
				break
	return basic_block
	
def constant_folding(basic_block):
	"""
	Where possible, find out the result of operations and use this to
	eliminate computations from runtime by doing them compile time.

	This function uses a registers field in the basic block to know what
	register values are known beforehand in relation to this block.
	Execute multiple times to take advantage of more previous blocks.
	"""
	# A little hack to find out when the first 'previous' is processed
	registers = None
	for previous_block in basic_block.preceding:
		if previous_block.jal:

			# TODO: saved registers?
			registers = {}
		if registers == None:
			registers = previous_block.constant_folding_registers
		else:
			for name in previous_block.constant_folding_registers:
				if name in registers:
					if previous_block.constant_folding_registers[name] != registers[name]:
						del registers[name]
	if registers == None:
		# This can happen in case no previous block is available
		registers = {}

			
	instruction_location = -1
	# check out each operation in the basic block in a linear fashion
	for operation in basic_block.instructions:
		instruction_location += 1
		if operation.type == instruction.macro:
			break
		folded = False
		groups = operation.regex_match.groupdict()
		# Map interresting values.
		if 'rd' in groups:
			rd = operation.regex_match.group('rd')
		if 'rs' in groups:
			rs = operation.regex_match.group('rs')
		if 'rt' in groups:
			rt = operation.regex_match.group('rt')
			
		# A 'value' means the value for the destination register is known.
		# (Basically it means it is a load_immediate operation.)
		if operation.__dict__.has_key('value'):
			folded = True
			registers[rd] = operation.value
			
		# Instructions with an immediate can be computed if the source register is
		# known beforehand.
		elif (operation.is_rs_computable() and
				registers.has_key(rs)):
			folded = True
			registers[rd] = operation.compute_immediate(registers[rs])
		
		# Instrictions with an rs and rt register argument can be computed if rs
		# and rt are known at compile time
		elif (operation.is_rsrt_computable() and
				registers.has_key(rs) and
				registers.has_key(rt)):
			folded = True
			registers[rd] = operation.compute_rsrt(registers[rs], registers[rt])
		# register computable implies the instruction could
		# compute results which alter the state of a certain
		# register, rather then a 'destination register'. Thus,
		# this instruction needs to have access to whatever
		# register that may be; easiest done by giving it _all_
		# register and let it alter the state
		elif (operation.is_register_computable()):
			result = operation.compute_registers(registers)
			# None implies no calculation is done
			if result != None:
				# lter the register state afterwards
				registers = result

		# If the instruction cannot be computed, the result register cannot be
		# trusted any more
		else:
			if 'rd' in groups:
				if rd in registers:
					del registers[rd]

		# TODO: change instructions to LI
		if folded:
			string = "li " + rd.__str__() + "," + registers[rd].__str__()
			replacement = instruction.parse(string)
			basic_block.instructions[instruction_location] = replacement
	basic_block.constant_folding_registers = registers
	return basic_block
	
def check_instructions(write_list, instruction_list):
	useful = None
	for operation in instruction_list:
		if useful == None:
			if registers.get_intersection(write_list, operation.read):
				useful = True
			elif registers.get_intersection(write_list, operation.write):
				useful = False
	return useful
		

def dead_code_elimination(basic_block):
	"""
	If an instruction writes to a register, and that register is written again
	before it is read, the instruction is plain useless and should be removed
	"""
	instruction_list = basic_block.instructions
	index = 0
	# index iterates over all instructions within the given basic block.
	while index < len(instruction_list):
		useful = None

		write_list = instruction_list[index].write
		
		# leave alone macro's/pseudo-ops; they are always considered useful
		if instruction_list[index].type == instruction.macro:
			useful = True
		# noops are never considered useful:
		if instruction_is_noop(instruction_list[index]):
			useful = False
		
		# Check the written registers with the rest of the list.
		if useful == None:
			useful = check_instructions(write_list, instruction_list[index + 1:])

		# Check liveness with JAL or Branch instructions (these are mutually
		# exclusive)
		if useful == None:
			if basic_block.jal:
				dict = registers.jal_liveness()
				dead = dict['dead']
				live = dict['live']
				# an empty list is considered false
				if registers.get_intersection(write_list, dead):
					useful = False
				if registers.get_intersection(write_list, live):
					useful = True
			elif basic_block.branch:
			  reads = basic_block.get_branch_reads()
			  if registers.get_intersection(write_list, reads):
				  useful = True
		# check instruction with the liveness analysis information of following
		# blocks.
		if useful == None:
			live = []
			dead = registers.all
			for exit in basic_block.get_exit_points():
				if exit == "$31":
					exit = registers.jal_return()
				# a live register in any path is live.
				live = registers.get_union(live, exit.live)
				# A register is only dead if it is dead everywhere
				dead = registers.get_intersection(dead, exit.dead)
			if registers.get_intersection(write_list, dead):
				useful = False
			if registers.get_intersection(write_list, live):
				useful = True

		# We don't know about the undetermined instructions usefulness, so we
		# leave them alone. The useless ones can be deleted.
		if useful == False:
			del instruction_list[index]
		else:
			# it only makes sense to continue one instruction if the list has
			# not changed.
			index += 1
	return basic_block

def optimize_block_structure(block_structure):
	"""
	Processes all basic blocks in a block structure leaving other parts of it
	alone.
	"""
	for iteration in xrange(options.get_option("iterations")):
		for optimize in (
				common_subexpression_elimination,
				constant_folding,
				dead_code_elimination):
			for x in xrange(len(block_structure)):
				block_element = block_structure[x]
				if block_element[0] == "block":
					# first reset the the liveness e.d.
					basic_block.reset_liveness(block_element[2])
					constant_folding_registers(block_element[2])
					for y in xrange(len(block_element[2])):
						block = block_element[2][y]
						for iteration in xrange(len(block_structure)):
							block = optimize(block)
						block_structure[x][2][y] = block		
	return block_structure
			
# vim: foldmethod=indent:foldlevel=0
