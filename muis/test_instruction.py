#!/usr/bin/env python
# Code released under a free software compatible license. See LICENSE for more
# details.
# Original author: Nido Media
# Code patches by:
#	...


import unittest
import instruction
import re

whitespace = re.compile("^\s*$")
	
class testSequenceFunctions(unittest.TestCase):
	def setUp(self):
		self.test_instructions = open("../code/supported.s").read().split("\n")

	def testInstructionParsing(self):
		for test in self.test_instructions:
			# only test cases with actual code
			if not whitespace.match(test):
				result = instruction.parse(test)
				#print result 
				#print result.read
				#print result.write
				# there is a result (ass in, all 'supported' must be parsed
				# correctly.
				self.assert_(result)

if __name__ == '__main__':
	unittest.main()
