"""
muis.py reads an assembly file and turns it into a basic block
structure. This basic block structure is then parsed back to assembly
and printed to file.

Usage: 
	python muis.py in_file out_file
or, if you have python at /usr/bin/python you can call muis.py directly.
	./muis.py in_file out_file

Code released under a free software compatible license. See LICENSE for more
details.
Original author: Nido Media
Code patches by:
	...
"""


import re
import sys

import aliasdb
import basic_block
import convenience
import instruction


new_var_count = 0

# A few varialbes for the tuple structure.
tuple_type = 0
tuple_block_name = 1
tuple_block_content = 2

def assert_pseudo_block(block):
	"""
	Checks a pseudo block for integrity
	"""
	assert block.has_key('name')
	assert block['name'] != None
	assert block.has_key('code')
	assert block.has_key('jump')

def eliminate_comment(string):
	"""
	Removes the comment from a line of assembly code.
	Returns nothing if the line is empty or has only comment.
	Returns the full line when no comment found.
	Returns commentless line when comment found.
	"""
	comment = re.compile('#.*')
	empty_line = re.compile('^\s*$')
	
	line_match = comment.search(string)
	# eliminate comment from the string
	if line_match:
		string = line_match.string[ : line_match.start() ]
	
	# return only when the commentless line has 'code'.
	if not empty_line.match(string):
		return string
		
def open_file(file_name):
	"""
	Open the file, return the file as a string, or die when the file cannot be
	found.
	"""
	try:
		opened_file = file(file_name)
		string = opened_file.read()
		return(string.splitlines())
	except IOError:
		print >> sys.stderr, ('IOError: probably the file "' + file_name +
				'" cannot not found; exiting.')
		sys.exit(1)
	assert False, "Unreachable code"

def create_new_work_block(name = None):
	"""
	Creates a skeleton pseudoblock. If a name is given, this will be the name of
	the new block. Otherwise, a new name will be generated on the fly.
	"""
	if None == name:
		name = get_new_block_name()
	block = {}
	block['name'] = name
	block['code'] = []
	return block

def read_assembly_file(file_name):
	"""
	Reads an assembly file and puts it into "label blocks".
	The difference between pseudo blocks and these blocks is that these
	blocks are created with respect to jump labels rather then jumps
	themselves. True basic blocks are created later
	"""
	results = []

	assembly_lines = []

	input_lines = open_file(file_name)

	# Turn the file into a list of strings and eliminate comment and empty lines
	for line in input_lines:
		line = eliminate_comment(line)
		if line:
			assembly_lines.append(line)
	
	# Initialise the alias database so this doesn't have to be hacked in in
	# later code.
	aliasdb.create_database(assembly_lines)

	# Process the data within .ent/.end fields, these are 'top level' blocks.
	# (they correspond with functions). Also, their structure is important to
	# the assembly format. Code outside this is ignored as this is mostly
	# pseudo-operations. Code inside is considered independent of eachother
	
	function_entrance = re.compile("\s*\.ent\s*(?P<function_name>[\w\.]*)\s*")
	function_end = re.compile("\s*\.end\s*(?P<function_name>[\w\.]*)\s*")

	current_function = None
	loose_assembly = []
	ent_line = None
	end_line = None
	for index in xrange(len(assembly_lines)):
		line = assembly_lines[index]
		
		ent = function_entrance.search(line)
		if ent:
			assert end_line == None, "Found a begin before the old one ended"
			current_function = ent.group('function_name')
			ent_line = index
	
		end = function_end.search(line)
		if end:
			assert ent_line != None, "found an end but miss a beginning"
			assert current_function == end.group('function_name')
			end_line = index
			
		if ent_line and end_line:
			block = assembly_function_block_to_basic_blocks(assembly_lines[ent_line + 1:end_line])
			results.append( ("block", current_function, block) )
			# reset values
			current_function = None
			ent_line = None
			end_line = None
			
		# when not working with the function blocks, we want to aggregate the
		# loose lines, and add them to our structure as well.
		# elif instead of just 'if' because the last lines of the previous if
		# make this value true
		elif not (ent_line or end_line):
			loose_assembly.append(line)

		elif loose_assembly != []:
			results.append(("assembly", loose_assembly))
			loose_assembly = []
	return results	
	
def assembly_function_block_to_basic_blocks(assembly_lines):
	"""
	We sequentially walk through all lines creating new pseudoblocks once we
	find labels. These labels tell us there a new basic block is created. Then
	the 'current' basic block will be written to the basic block list and the
	rest of the data will be added to the new block.
	"""
	basic_blocks = []
	work_block = create_new_work_block()
	
	for line in assembly_lines:
		if re.search(":", line):
			# a new label begins now (labels are identified by the ":"), so we
			# save the old pseudo_nlock and continue with a new one.
			new_block_name = line.strip()[:-1]

			work_block['jump'] = new_block_name
			basic_blocks.append(work_block)
			work_block = create_new_work_block(new_block_name)
			
		else:
			work_block['code'].append(line)
			
	# The last block must be appended to the basic block list as well, with a
	# fake jump argument.
	work_block['jump'] = ""
	basic_blocks.append(work_block)

	return basic_blocks

def print_pseudo_blocks(basic_blocks):
	"""
	Prints the basic blocks structure in a way usable for debugging; no "real"
	functionality.
	"""
	for basic_block in basic_blocks:
		assert_pseudo_block(basic_block)
		# the very first block is nameless most of the time
		if basic_block.has_key('name'):
			print "Basic Block: " + basic_block['name']
		for line in basic_block['code']:
			print '\t' + line
		if basic_block.has_key('jump'):
			print 'Jumps to: ' + basic_block['jump']
		if basic_block.has_key('jump_and_link'):
			print 'Jump and link: ' + basic_block['jump_and_link']
		if basic_block.has_key('branch'):
			print 'Branch instruction: ' + basic_block['branch']
		print '\n'

def create_pseudo_blocks(input):
	"""
	Creates full pseudo blocks from a structure with a label block
	structure created by read_assembly file.
	"""
	output = []
	# A check to see wether the input processing is a superfluous action.
	superfluous = False 
	#for basic_block in input:
	while len(input) != 0:
		basic_block = input[0]
		for line in basic_block['code']:
			if superfluous:
				break
			matcher = line.strip()
			# GoTo instructions are the ones beginning with either a 'j'
			# (jump instructions) or a b (branch instructions).
			if matcher[0] == 'j':
				if matcher[1].isspace():
					# the instruction is j<space>; which make it an
					# unconditional jump instruction.
					# create jump exit. These jumps usually happen before a
					# label. If this is not the case, the code behind the jump
					# is unreachable.
					
					# store the jump location
					basic_block['jump'] = matcher[1 : ].strip()

					#update the basic block to not include dead code from after the jump.
					jump_point = basic_block['code'].index(line)
					basic_block['code'] = basic_block['code'][:jump_point]
					# make sure no more code for this block is processed
					superfluous = True
					
				elif matcher[:3] == 'jal':
					# jal (jump and link) instructions fire up
					# procedures which might make use of any of the
					# available registers. Though there is a convention
					# of which registers should be used temporarily and
					# which on a global scale, compilers generally
					# ignore this convention; hence jal instructons
					# should be treated as a jump and the block must be
					# split.
					basic_block['jump_and_link'] = matcher[3 : ].strip()
					new_basic_block = create_new_work_block()
			
					# The current basic block will jump to the new basic block.
					# The new basic block will jump to the old jump target
					new_basic_block['jump'] = basic_block['jump']
					basic_block['jump'] = new_basic_block['name']
			
					# Code before the Jump And Link are part of the old basic
					# block. Code after is are part of the new one.
					split = basic_block['code'].index(line)
					new_basic_block['code'] = basic_block['code'][split + 1:]
					del basic_block['code'][split : ]
					
					# Add the newly created block to the list of blocks to be
					# processed.
					input.append(new_basic_block)
			if matcher[0] == 'b':
				# branch instructions conditionally start a procedure,
				# so the basic block must be split and the branch
				# instruction has information and thus should be saved.
				basic_block['branch'] = line.split()
				new_basic_block = create_new_work_block()

				# forward the old jump instruction to the new
				# basic block and set the current jump instruction
				# to the new basic block itself.
				new_basic_block['jump'] = basic_block['jump']
				basic_block['jump'] = new_basic_block['name']
				
				# same as with jump and link
				split = basic_block['code'].index(line)
				new_basic_block['code'] = basic_block['code'][split + 1 : ]
				del basic_block['code'][split : ]
				
				input.append(new_basic_block)
		# Reset the superfluous value (made sure code can be processed again)
		superfluous = False
		# The block is cleaned, add it to the output.
		output.append(basic_block)
		input.remove(basic_block)
	return(output)

def optimise_remove_useless_moves(blocks):
	"""
	removes 'move' instructions of which the source and target are the same.
	"""
	move_recogniser = re.compile("move")
	for block in blocks:
		line = 0
		while line < len(block['code']):
			move = ["a","b"]
			if move_recogniser.search(block['code'][line]):
				move = block['code'][line].split()[-1]
				move = move.split(",")
			if move[0] == move[1]:
				del block['code'][line]
			else:
				line += 1

def get_new_block_name():
	"""
	Allows for the generation of new block names following the variable name
	new_var_count. Prepended with muis_temp. It assumes no muis_temp_[number]
	variables are in use before running this program.
	"""
	global new_var_count
	block_name = 'muis_temp_' + new_var_count.__str__()
	new_var_count += 1
	assert block_name != None
	return block_name

def assert_pseudo_block_list(block_list):
	"""
	Checks wether the pseudo blocks follow pseudo block rules on pseudo block
	level.
	"""
	for block in block_list:
		assert_pseudo_block(block)

def reader(filename):
	"""
	Function to call from this file. It will read the file identified as
	filename and returns its contents as basic blocks.
	"""
	in_file = filename

	# Create a pseudo-block structure of the assembly file
	blocks_structure = read_assembly_file(file_name = in_file)
		
	for index in xrange(len(blocks_structure)):
		tuple = blocks_structure[index]
		if tuple[tuple_type] == "block":
			blocks = tuple[tuple_block_content]
			assert_pseudo_block_list(blocks)
			blocks = process_pseudo_blocks(blocks)
			blocks_structure[index] = (tuple[tuple_type], tuple[tuple_block_name], blocks)
	return blocks_structure

def process_pseudo_blocks(blocks):
	"""
	Takes a list of pseudo blocks and does some pre-processing on them. Also
	turns them into `real' basic blocks and returns that.
	"""
	blocks = create_pseudo_blocks(blocks)
	# do some easy instruction deletion on pseudo block level
	optimise_remove_useless_moves(blocks)
	assert_pseudo_block_list(blocks)
			
	blocks = create_structured_basic_blocks(blocks)
	return blocks		
	

def create_structured_basic_blocks(blocks):
	"""
	Takes a list of pseudo blocks and turns them into real basic_blocks
	"""
	real_blocks = []
	for block in blocks:
		name = block['name']
		code = []
		for line in block['code']:
			code_line = instruction.parse(line)
			code.append(code_line)

		jump = block['jump']
		
		if block.has_key('branch'):
			branch = block['branch']
		else:
			branch = None

		if block.has_key('jump_and_link'):
			jal = block['jump_and_link']
		else:
			jal = None
			
		temp = basic_block.basic_block(name, code, jump, branch, jal,
			real_blocks)
		
		real_blocks.append(temp)
	return(real_blocks)

# vim:nocindent:nosmartindent:autoindent:tabstop=4:shiftwidth=4
# vim:textwidth=80:foldmethod=indent:foldlevel=0
