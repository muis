#!/usr/bin/env python
# Code released under a free software compatible license. See LICENSE for more
# details.
# Original author: Nido Media
# Code patches by:
#	...


import unittest
import instruction
import reader
import optimize
import basic_block
import re

whitespace = re.compile("^\s*$")
	
class testSequenceFunctions(unittest.TestCase):
	def setUp(self):
		global testfile
		global test_data
		testfile = "../code/basic_file.s"
		test_data = reader.reader(testfile)
		for tuple in test_data:
			if tuple[0] == "block":
				# just grab a random ol' block. nobody cares which one
				self.block = tuple[2][1]
		
	def testBlockLevelOptimization(self):
		block = self.block
		
		#optimize.temporary_variable(block)
		optimize.constant_folding(block) # done
		#optimize.algebraic_transformations(block)
		optimize.common_subexpression_elimination(block) #done
		#optimize.copy_propagation(block)
		#optimize.interchange_of_independent_statement(block)
		optimize.dead_code_elimination(block) # done
		
if __name__ == '__main__':
	unittest.main()
