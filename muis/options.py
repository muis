"""
Option getter and setter.

Options parses, well, options :). These can come from basically
anywhere. At time of creation, the options come from command line
arguments.

Code released under a free software compatible license. See LICENSE for more
details.
Original author: Nido Media
Code patches by:
	...
"""

import sys

option_set = {}

def is_set(argument):
	"""
	Checks if a certain option is set
	"""
	result = False
	if argument in option_set:
		result = True
	return result
	
def get_option(argument):
	"""
	Returns whatever given option is set to.
	"""
	result = None
	if is_set(argument):
		result = option_set[argument]
	else:
		print >> sys.stderr, "Argument " + argument.__str__() + " is not set."
	return result

def set(argument, value = True):
	"""
	Sets 'argument' to 'value'.
	"""
	option_set[argument] = value
	print option_set
