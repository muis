#!/bin/bash
# Compiles the file test.c and checks the output of the reference with the
# optimised version of the program. muis is the optimiser for this.
CC=xgcc
I386CC=gcc
CFLAGS=-O0
SIM="sim-outorder"
OPT=~/muis/muis/muis.py
OPTFLAGS=--agressive
IN=debug.c

X=true

$CC $CFLAGS -S $IN -o ref.s
$CC $CFLAGS ref.s -o ref

$OPT $OPTFLAGS ref.s opt.s || X=false

if $X
then
	$CC $CFLAGS opt.s -o opt

	echo simulating reference:
	$SIM ref 2>/dev/null | tee debug.ref 2>/dev/null || true
	echo
	echo simulating optimised:
	$SIM opt 2>/dev/null | tee debug.opt 2>/dev/null || true

	test -n "`diff debug.ref debug.opt`" && X=false
fi

$X && echo Success
$X || echo Failure

$X
