	.file	1 "debug.c"

 # GNU C 2.7.2.3 [AL 1.1, MM 40, tma 0.1] SimpleScalar running sstrix compiled by GNU C

 # Cc1 defaults:
 # -mgas -mgpOPT

 # Cc1 arguments (-G value = 8, Cpu = default, ISA = 1):
 # -quiet -dumpbase -O0 -o

gcc2_compiled.:
__gnu_compiled_c:
	.globl	Version
	.sdata
	.align	2
Version:
	.ascii	"1.1\000"
	.text
	.align	2
	.globl	main
	.sdata
	.align	2
$LC0:
	.ascii	"1\000"
	.align	2
$LC1:
	.ascii	"%s\000"
	.align	2
$LC2:
	.ascii	"looping\000"
	.text
	.align	2
	.globl	Proc0
	.align	2
	.globl	Func2
	.align	2
	.globl	Proc6
	.align	2
	.globl	Func3
	.align	2
	.globl	Func1

	.comm	IntGlob,4

	.comm	BoolGlob,4

	.comm	Array1Glob,204

	.comm	Array2Glob,10404

	.text

	.loc	1 48
	.ent	main
main:
	.frame	$fp,24,$31		# vars= 0, regs= 2/0, args= 16, extra= 0
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	subu	$sp,$sp,24
	sw	$31,20($sp)
	sw	$fp,16($sp)
	move	$fp,$sp
	jal	__main
	jal	Proc0
	move	$4,$0
	jal	exit
$L1:
	move	$sp,$fp			# sp not trusted here
	lw	$31,20($sp)
	lw	$fp,16($sp)
	addu	$sp,$sp,24
	j	$31
	.end	main

	.loc	1 62
	.ent	Proc0
Proc0:
	.frame	$fp,104,$31		# vars= 72, regs= 3/0, args= 16, extra= 0
	.mask	0xc0010000,-8
	.fmask	0x00000000,0
	subu	$sp,$sp,104
	sw	$31,96($sp)
	sw	$fp,92($sp)
	sw	$16,88($sp)
	move	$fp,$sp
	addu	$2,$fp,24
	move	$4,$2
	la	$5,$LC0
	jal	strcpy
	move	$16,$0
$L3:
	sltu	$2,$16,50
	bne	$2,$0,$L6
	j	$L4
$L6:
	la	$4,$LC1
	la	$5,$LC2
	jal	printf
	addu	$2,$fp,24
	addu	$3,$fp,56
	move	$4,$2
	move	$5,$3
	jal	Func2
	xori	$3,$2,0x0000
	sltu	$2,$3,1
	sw	$2,BoolGlob
$L5:
	addu	$16,$16,1
	j	$L3
$L4:
$L2:
	move	$sp,$fp			# sp not trusted here
	lw	$31,96($sp)
	lw	$fp,92($sp)
	lw	$16,88($sp)
	addu	$sp,$sp,104
	j	$31
	.end	Proc0

	.loc	1 84
	.ent	Func2
Func2:
	.frame	$fp,32,$31		# vars= 8, regs= 2/0, args= 16, extra= 0
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	subu	$sp,$sp,32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	move	$fp,$sp
	sw	$4,32($fp)
	sw	$5,36($fp)
	li	$2,0x00000001		# 1
	sw	$2,16($fp)
$L8:
	lw	$2,16($fp)
	slt	$3,$2,2
	bne	$3,$0,$L10
	j	$L9
$L10:
	lw	$2,32($fp)
	lw	$4,16($fp)
	addu	$3,$2,$4
	lb	$2,0($3)
	lw	$3,16($fp)
	lw	$4,36($fp)
	addu	$3,$3,$4
	addu	$4,$3,1
	lb	$3,0($4)
	move	$4,$2
	move	$5,$3
	jal	Func1
	bne	$2,$0,$L11
	lw	$3,16($fp)
	addu	$2,$3,1
	move	$3,$2
	sw	$3,16($fp)
$L11:
	j	$L8
$L9:
	lb	$2,20($fp)
	li	$3,0x00000058		# 88
	bne	$2,$3,$L12
	li	$2,0x00000001		# 1
	j	$L7
	j	$L13
$L12:
	move	$2,$0
	j	$L7
$L13:
$L7:
	move	$sp,$fp			# sp not trusted here
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addu	$sp,$sp,32
	j	$31
	.end	Func2

	.loc	1 110
	.ent	Proc6
Proc6:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, extra= 0
	.mask	0x40000000,-8
	.fmask	0x00000000,0
	subu	$sp,$sp,8
	sw	$fp,0($sp)
	move	$fp,$sp
	sw	$4,8($fp)
	sw	$5,12($fp)
	lw	$2,12($fp)
	lw	$3,8($fp)
	sw	$3,0($2)
	lw	$2,8($fp)
	sltu	$3,$2,5
	beq	$3,$0,$L23
	lw	$2,8($fp)
	move	$3,$2
	sll	$2,$3,2
	la	$3,$L22
	addu	$2,$2,$3
	lw	$3,0($2)
	j	$3
	.rdata
	.align	3
	.align	2
$L22:
	.word	$L16
	.word	$L17
	.word	$L19
	.word	$L20
	.word	$L21
	.text
$L16:
	lw	$2,12($fp)
	sw	$0,0($2)
	j	$L15
$L17:
	lw	$2,IntGlob
	slt	$3,$2,101
	bne	$3,$0,$L18
	lw	$2,12($fp)
	sw	$0,0($2)
$L18:
$L19:
	lw	$2,12($fp)
	li	$3,0x00000001		# 1
	sw	$3,0($2)
	j	$L15
$L20:
	j	$L15
$L21:
	lw	$2,12($fp)
	li	$3,0x00000002		# 2
	sw	$3,0($2)
$L23:
$L15:
$L14:
	move	$sp,$fp			# sp not trusted here
	lw	$fp,0($sp)
	addu	$sp,$sp,8
	j	$31
	.end	Proc6

	.loc	1 125
	.ent	Func3
Func3:
	.frame	$fp,16,$31		# vars= 8, regs= 1/0, args= 0, extra= 0
	.mask	0x40000000,-8
	.fmask	0x00000000,0
	subu	$sp,$sp,16
	sw	$fp,8($sp)
	move	$fp,$sp
	sw	$4,16($fp)
	lw	$2,16($fp)
	sw	$2,0($fp)
	move	$2,$0
	j	$L24
$L24:
	move	$sp,$fp			# sp not trusted here
	lw	$fp,8($sp)
	addu	$sp,$sp,16
	j	$31
	.end	Func3

	.loc	1 134
	.ent	Func1
Func1:
	.frame	$fp,16,$31		# vars= 8, regs= 1/0, args= 0, extra= 0
	.mask	0x40000000,-8
	.fmask	0x00000000,0
	subu	$sp,$sp,16
	sw	$fp,8($sp)
	move	$fp,$sp
	move	$3,$4
	move	$4,$5
	sb	$3,0($fp)
	sb	$4,1($fp)
	move	$2,$0
	j	$L25
$L25:
	move	$sp,$fp			# sp not trusted here
	lw	$fp,8($sp)
	addu	$sp,$sp,16
	j	$31
	.end	Func1
