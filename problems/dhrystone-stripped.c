#define LOOPS	50
#undef	NOENUM
#undef	NOSTRUCTASSIGN
#define TIMES
#define HZ	100	
char	Version[] = "1.1";


typedef enum	{Ident1, Ident2, Ident3, Ident4, Ident5} Enumeration;

typedef int	OneToThirty;
typedef int	OneToFifty;
typedef char	CapitalLetter;
typedef char	String30[31];
typedef int	Array1Dim[51];
typedef int	Array2Dim[51][51];

struct	Record
{
	struct Record		*PtrComp;
	Enumeration		Discr;
	Enumeration		EnumComp;
	OneToFifty		IntComp;
	String30		StringComp;
};

typedef struct Record 	RecordType;
typedef RecordType *	RecordPtr;
typedef int		boolean;

#define	NULL		0
#define	TRUE		1
#define	FALSE		0

#ifndef REG
#define	REG
#endif

extern Enumeration	Func1();
extern boolean		Func2();

#ifdef TIMES
#include <sys/types.h>
#include <sys/times.h>
#endif

main()
{
	Proc0();
	exit(0);
}

/*
 * Package 1
 */
int		IntGlob;
boolean		BoolGlob;
Array1Dim	Array1Glob;
Array2Dim	Array2Glob;

Proc0()
{
	REG char		CharLoc;
	Enumeration	 	EnumLoc;
	String30		String1Loc;
	String30		String2Loc;

#ifdef TIMES
	register unsigned int	i;

#endif

	strcpy(String1Loc, "DHRYSTONE PROGRAM, 1'ST STRING");
	for (i = 0; i < LOOPS; ++i)
	{
		printf("%s", "looping");
		BoolGlob = ! Func2(String1Loc, String2Loc);
	}

}


boolean Func2(StrParI1, StrParI2)
String30	StrParI1;
String30	StrParI2;
{
	REG OneToThirty		IntLoc;
	REG CapitalLetter	CharLoc;


	IntLoc = 1;
	while (IntLoc <= 1)
		if (Func1(StrParI1[IntLoc], StrParI2[IntLoc+1]) == Ident1)
		{
			++IntLoc;
		}
	if (CharLoc == 'X')
		return(TRUE);
	else
	{
		if (strcmp(StrParI1, StrParI2) > 0)
		{
			return (TRUE);
		}
		else
			return (FALSE);
	}
}



extern boolean Func3();

Proc6(EnumParIn, EnumParOut)
REG Enumeration	EnumParIn;
REG Enumeration	*EnumParOut;
{
	*EnumParOut = EnumParIn;
	switch (EnumParIn)
	{
	case Ident1:	*EnumParOut = Ident1; break;
	case Ident2:	if (IntGlob > 100) *EnumParOut = Ident1;
	case Ident3:	*EnumParOut = Ident2; break;
	case Ident4:	break;
	case Ident5:	*EnumParOut = Ident3;
	}
}

boolean Func3(EnumParIn)
REG Enumeration	EnumParIn;
{
	REG Enumeration	EnumLoc;

	EnumLoc = EnumParIn;
	return (FALSE);
}

Enumeration Func1(CharPar1, CharPar2)
CapitalLetter	CharPar1;
CapitalLetter	CharPar2;
{
		return (Ident1);
}
