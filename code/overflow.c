#include <stdio.h>

#define BIG_NUMBER 2000000000
int main(int argc, char* argv[]){
	/* Shows overflow behaviour */
	int i;
	int x;
	unsigned int y;

   	x = BIG_NUMBER;
	y = BIG_NUMBER;

	x += BIG_NUMBER;
	y += BIG_NUMBER; 

	x += BIG_NUMBER;
	y += BIG_NUMBER; 

	x += BIG_NUMBER;
	y += BIG_NUMBER; 

	x += BIG_NUMBER;
	y += BIG_NUMBER; 

	x += BIG_NUMBER;
	y += BIG_NUMBER; 

	x += BIG_NUMBER;
	y += BIG_NUMBER; 

	x += BIG_NUMBER;
	y += BIG_NUMBER; 

	x += BIG_NUMBER;
	y += BIG_NUMBER; 

	x += BIG_NUMBER;
	y += BIG_NUMBER; 

	x += BIG_NUMBER;
	y += BIG_NUMBER; 

	printf("%i\n", x);
	printf("%u\n", y);
}
